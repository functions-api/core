const TestHandler = require('./test-handler.js')
const JsonBodyHandler = require('../../lib/handlers/json.js')(TestHandler)
const Errors = require('../../lib/errors.js')

describe('JsonBodyHandler', () => {
    var opts = {
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({hello: 'world'})
    }
    it('throws an error if the content-type is not json', () => {
        let badHeaders = Object.assign({}, opts, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        let call = () => { new JsonBodyHandler(badHeaders) }
        expect(call).toThrow(Errors.BadRequestError)
    })

    it('throws an error for invalid JSON', () => {
        let badBody = Object.assign({}, opts, {
            body: 'test'
        })
        let call = () => { new JsonBodyHandler(badBody) }
        expect(call).toThrow(Errors.BadRequestError)

        try {
            call()
        } catch(e) {
            expect(e.invalidParams[0].reason).toBe('Unexpected token e in JSON at position 1')
        }
    })

    test('sets the body to the parsed JSON body', () => {
        var handler = new JsonBodyHandler(opts)
        expect(handler.body).toEqual({hello: 'world'})
    })
})