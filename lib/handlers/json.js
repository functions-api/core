const Errors = require('../errors.js')

module.exports = function(BaseHandler) {
    const BodyHandler = require('./body.js')(BaseHandler)
    return class JsonBodyHandler extends BodyHandler {
        constructor (...args) {
            super(...args)

            var contentType = this.headers['Content-Type'] || this.headers['content-type']
            if (contentType !== 'application/json') {
                const received = contentType ? `Received \`${contentType}\`.` : 'Received header was blank.'
                throw new Errors.BadRequestError('Invalid Content-Type header', [{
                    name: 'Content-Type',
                    reason: `Content-Type must be \`application/json\`. ${received}`
                }])
            }

            try {
                this.body = JSON.parse(this.body)
            } catch(e) {
                const reason = e.name === 'SyntaxError' ? e.message : 'Could not parse JSON'
                throw new Errors.BadRequestError('The request body contained invalid JSON', [{
                    reason,
                    name: 'body',
                }])
            }
        }
    }
}