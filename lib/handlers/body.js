const Errors = require('../errors.js')

module.exports = function(BaseHandler) {
    return class BodyHandler extends BaseHandler {
        constructor (...args) {
            super(...args)

            if (this.body === undefined || this.body.length === 0) {
                throw new Errors.BadRequestError('Request body is required.')
            }
        }
    }
}