const AbstractHandler = require('../../lib/handlers/abstract.js')

module.exports = class TestHandler extends AbstractHandler {
    constructor(opts) {
        super()
        this.opts = opts || {}
        super.init()
    }

    setBody() {
        this.body = this.opts.body || AbstractHandler.prototype.setBody.call(this)
    }

    setIdentity() {
        this.identity = this.opts.identity || 'anidentity'
    }

    setHeaders() {
        this.headers = this.opts.headers || AbstractHandler.prototype.setHeaders.call(this)
    }

    setParams() {
        AbstractHandler.prototype.setParams.call(this)
        if (this.opts.pathParams) this.pathParams = this.opts.pathParams
        if (this.opts.queryParams) this.queryParmas = this.opts.queryParams
    }
}