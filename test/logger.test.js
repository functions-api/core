const Logger = require('../lib/logger.js')

describe('Logger', () => {
  describe('setup', () => {
    it('defines methods for each of the logging thresholds', () => {
      expect(Logger.prototype).toHaveProperty('error')
      expect(Logger.prototype).toHaveProperty('warn')
      expect(Logger.prototype).toHaveProperty('info')
      expect(Logger.prototype).toHaveProperty('debug')
      expect(Logger.prototype).toHaveProperty('verbose')
      expect(Logger.prototype).toHaveProperty('silly')
    })
  })

  describe('logging', () => {
    it ('sets a default threshold based on environment', () => {
      const prevEnv = process.env.NODE_ENV

      process.env.NODE_ENV = 'test'
      expect(new Logger().threshold).toBe(0)

      process.env.NODE_ENV = 'development'
      expect(new Logger().threshold).toBe(3)

      process.env.NODE_ENV = 'production'
      expect(new Logger().threshold).toBe(2)

      process.env.NODE_ENV = 'afakeenv'
      expect(new Logger().threshold).toBe(2)

      process.env.NODE_ENV = prevEnv
    })

    it('allows setting a custom threshold', () => {
      expect(new Logger(5).threshold).toBe(5)
    })

    it('only logs <= the set threshold', () => {
      const log = new Logger(2)
      const logMock = jest.fn()
      console.log = logMock

      log.log(3, 'test 3')
      expect(logMock).not.toHaveBeenCalled()
      
      logMock.mockClear()
      log.log(1, 'test 1')
      expect(logMock).toHaveBeenCalled()

      logMock.mockClear()
      log.log(2, 'test 2')
      expect(logMock).toHaveBeenCalled()

      console.log.mockRestore()
    })

    it('convenience functions call log', () => {
      const log = new Logger()
      const logMock = jest.fn()
      log.log = logMock

      log.warn('Hi')
      expect(logMock).toHaveBeenCalledWith(1, 'Hi')

      log.log.mockRestore()
    })
  })
})