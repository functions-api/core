const LEVELS = [
  'error',
  'warn',
  'info',
  'debug',
  'verbose',
  'silly'
]

class Logger {
  constructor(logThreshold) {
    if (logThreshold !== undefined) {
      if (typeof(logThreshold) === 'string') {
        this.threshold = LEVELS.indexOf(logThreshold)
      } else if (typeof(logThreshold) === 'number') {
        this.threshold = logThreshold
      } else {
        throw new Error('Log Threshold must by a number or a string.')
      }
    } else {
      switch (process.env.NODE_ENV) {
        case 'test':
          this.threshold = LEVELS.indexOf('error')
          break

        case 'development':
          this.threshold = LEVELS.indexOf('debug')
          break

        default:
          this.threshold = LEVELS.indexOf('info')
          break
      }
    }
  }

  log(level, ...messages) {
    if (level <= this.threshold) {
      console.log(...messages)
    }
  }
}

LEVELS.forEach((level, index) => {
  if (Logger.prototype[level] !== undefined) {
    throw new Error(`Log level ${level} already exists on Logger.`)
  }
  Logger.prototype[level] = function(...args) {
    this.log(index, ...args)
  }
})

module.exports = Logger