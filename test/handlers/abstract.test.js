const TestHandler = require('./test-handler.js')
const AbstractHandler = require('../../lib/handlers/abstract.js')
const Errors = require('../../lib/errors.js')

describe("AbstractHandler", () => {
    describe('initialization', () => {
        test('it has a logger instance on the base class', () => {
            expect(AbstractHandler).toHaveProperty('logger')
        })
    })

    describe('abstractions', () => {
        test('throws an error when run is not defined', () => {
            class FakeHandler extends AbstractHandler {}
            let handler = new FakeHandler()
            expect(handler.run).toThrow('No run method defined')
        })
    })

    describe('handler', () => {
        it('creates an instance of the child class', () => {
            let called = false
            class FakeHandler extends AbstractHandler {
                constructor(...args) {
                    super(...args)
                    called = true
                }
            }
            
            FakeHandler.handler()
            expect(called).toBeTruthy()
        })

        it('calls run', () => {
            let called = false
            class FakeHandler extends AbstractHandler {
                run() { called = true }
            }

            FakeHandler.handler()
            expect(called).toBeTruthy()
        })

        it('handles errors during run', () => {
            let called = false
            class FakeHandler extends AbstractHandler {
                run() { throw new Error() }
                static handleErrors(e) { called = true }
            }

            FakeHandler.handler()
            expect(called).toBeTruthy()
        })
    })

    test('sets the appropriate content-type for errors', () => {
        let resp = AbstractHandler.handleErrors(new Errors.RequestError('Test Error', [{
            name: 'test',
            reason: 'test reason',
        }]))

        expect(resp.headers['Content-Type']).toBe('application/problem+json')
    })

    test('handles request errors gracefully', () => {
        let resp = AbstractHandler.handleErrors(new Errors.RequestError('Test Error', [{
            name: 'test',
            reason: 'test reason',
        }]))

        expect(resp.statusCode).toBe(400)
        expect(typeof(resp.body)).toBe('string')
        let body = JSON.parse(resp.body)
        expect(body.title).toBe('RequestError')
        expect(body.detail).toBe('Test Error')
        expect(body.invalid_params.length).toBe(1)
        expect(body.invalid_params[0].name).toBe('test')
        expect(body.invalid_params[0].reason).toBe('test reason')
    })

    test('handles request errors with no invalidParams', () => {
        let resp = AbstractHandler.handleErrors(new Errors.RequestError('Generic Error'))

        expect(resp.statusCode).toBe(400)
        expect(typeof(resp.body)).toBe('string')
        let body = JSON.parse(resp.body)
        expect(body.title).toBe('RequestError')
        expect(body.detail).toBe('Generic Error')
        expect(body).not.toHaveProperty('invalid_params')
    })

    test('handles granular request errors gracefully', () => {
        let resp = AbstractHandler.handleErrors(new Errors.NotFoundError('Test NF Error', [{
            name: 'test',
            reason: 'test reason'
        }]))

        expect(resp.statusCode).toBe(404)
        expect(typeof(resp.body)).toBe('string')
        let body = JSON.parse(resp.body)
        expect(body.title).toBe('NotFoundError')
        expect(body.detail).toBe('Test NF Error')
        expect(body.invalid_params.length).toBe(1)
        expect(body.invalid_params[0].name).toBe('test')
        expect(body.invalid_params[0].reason).toBe('test reason')
    })

    test('throw fatal errors', () => {
        var erroredCall = AbstractHandler.handleErrors.bind(AbstractHandler, new Error('test'))
        expect(erroredCall).toThrow('test')
    })
})