const Errors = require('../errors.js')
const Logger = require('../logger.js')

class AbstractHandler {
    init (providedOptions) {
        this.options = { ...providedOptions }
        this.logger = new Logger(this.options.logLevel)
        this.setIdentity()
        this.setHeaders()
        this.setParams()
        this.setBody()
    }

    setBody() {
        this.body = undefined
    }

    setIdentity() {
        this.identity = null
    }

    setHeaders() {
        this.headers = {}
    }

    setParams() {
        this.pathParams = {}
        this.queryParmas = {}
    }

    run () {
        throw new Error('No run method defined')
    }

    static defaultParameters () {
        return {}
    }

    async providedParameters () {
        return {}
    }

    async buildParameters() {
        var providedParams = await this.providedParameters()
        return Object.assign({}, this.constructor.defaultParameters(), providedParams)  
    }

    static handleErrors(e) {
        if (Object.keys(Errors).indexOf(e.name) >= 0) {
            let body = {
                title: e.name,
                status: e.statusCode,
                detail: e.message
            }
            if (e.invalidParams.length) {
                body.invalid_params = e.invalidParams
            }

            return {
                statusCode: e.statusCode,
                headers: {
                    'Content-Type': 'application/problem+json'
                },
                body: JSON.stringify(body)
            }
        }

        AbstractHandler.logger.error(`Fatal Error ${e}`)
        throw e
    }

    static async handler(...args) {
        try {
            let func = new this(...args)
            return await func.run()
        } catch(e) {
            return this.handleErrors(e)
        }
    }
}

AbstractHandler.baseType = 'core'
AbstractHandler.logger = new Logger()
module.exports = AbstractHandler