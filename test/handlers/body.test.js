const TestHandler = require('./test-handler.js')
const AbstractBodyHandler = require('../../lib/handlers/body.js')
const AbstractHandler = require('../../lib/handlers/abstract.js')
const Errors = require('../../lib/errors.js')

describe('BodyHandler', () => {
    const BodyHandler = AbstractBodyHandler(TestHandler)
    
    test('it has a logger instance on the base class', () => {
        expect(BodyHandler).toHaveProperty('logger')
    })

    test('throws an error if no body is defined', () => {
        var call = () => { new BodyHandler() }
        expect(call).toThrow(Errors.RequestError)
    })

    test('throws an error if the body is an empty string', () => {
        var call = () => { new BodyHandler({body: ''}) }
        expect(call).toThrow(Errors.RequestError)
    })

    test('sets the body to the event body', () => {
        var handler = new BodyHandler({body: 'test'})
        expect(handler.body).toBe('test')
    })
})