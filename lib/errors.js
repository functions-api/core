class RequestError extends Error {
    constructor(detail, params) {
        super(detail)

        const invalidParams = params || []

        invalidParams.forEach(param => {
            if (typeof(param.name) !== 'string') {
                throw new Error("Error param must have a `name` field with the string type.")
            }

            if (typeof(param.reason) !== 'string') {
                throw new Error("Error param must have a `reason` field with the string type.")
            }
        })

        this.name = 'RequestError'
        this.statusCode = 400
        this.invalidParams = invalidParams
    }

    toString() {
        return JSON.stringify(this.invalidParams)
    }
}

class BadRequestError extends RequestError {
    constructor(message, params) {
        super(message, params)
        this.name = 'BadRequestError'
    }
}

class NotFoundError extends RequestError {
    constructor(message, params) {
        super(message, params)
        this.name = 'NotFoundError'
        this.statusCode = 404
    }
}

module.exports = {
    RequestError,
    BadRequestError,
    NotFoundError
}